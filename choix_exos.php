<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Learn English</title>

    <link rel="stylesheet" href="Style.css">

    <style>
        h2 {
            color: rgb(69, 62, 194);
            font-size: 5ch;
        }
        h3 {
            color: rgb(228, 128, 34);
            font-size: 4ch;
        }

        input[type=submit] {
            font-family: "kid_font";
            padding: 16px 32px;
            font-size: 28px;
            margin: 4px 2px;
            transition-duration: 0.4s;
            cursor: pointer;
        }

        #buttonCancel {
            font-family: "kid_font";
            padding: 16px 32px;
            font-size: 20px;
            margin: 4px 2px;
            cursor: pointer;
        }

        #button1 {
            background-color: white; 
            color: #4CAF50; 
            border: 2px solid #4CAF50;
        }
        #button1:hover {
            background-color: #4CAF50;
            color: white;
        }
        #button2 {
            background-color: white; 
            color: #008CBA; 
            border: 2px solid #008CBA;
        }
        #button2:hover {
            background-color: #008CBA;
            color: white;
        }
        #button3 {
            background-color: white; 
            color: #f44336; 
            border: 2px solid #f44336;
        }
        #button3:hover {
            background-color: #f44336;
            color: white;
        }
    </style>
</head>
<body>
    <?php

    if (!empty($_POST)) {  
        if (isset($_POST['Word_->_Song'])) {
            header("Location:  http://localhost/Song_Word.php?id=".htmlspecialchars($_GET["id"])."&theme=".htmlspecialchars($_GET["theme"]));
            exit();
        } elseif (isset($_POST['Image_->_Song'])) {
            header("Location:  http://localhost/Song_Image.php?id=".htmlspecialchars($_GET["id"])."&theme=".htmlspecialchars($_GET["theme"])); 
            exit();
        } else {
            header("Location:  http://localhost/Word_Image.php?id=".htmlspecialchars($_GET["id"])."&theme=".htmlspecialchars($_GET["theme"])); 
            exit();
        } 
    }

    ?>

    <table align="center" width="100%">
        <tr>
            <td align="left"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
            <td width="35%" align="right"><h1>Learn English</h1></td>
            <td width="25%" align="left"><img id="english_flag" src="https://acegif.com/wp-content/gifs/british-flag-31.gif" alt="english_flag"></td>
            <td align="right"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
        </tr>
    </table>

    <div id="main">
        <table id="main_table" align="center" width="50%" height="300px">
            <tr align="center" height="160px">
                <td colspan="2" valign="bottom"><h2><?php echo htmlspecialchars($_GET["theme"]);  ?></h2></td>
            </tr>
            <tr align="center">
                <td colspan="2"><h3>Choose an exercise</h3></td>
            </tr>
            <form method="POST">
            <tr align="center">
                <td colspan="2"><input id="button1" type="SUBMIT" name="Image -> Word" value="Word to Image"></td>
            </tr>
            <tr align="center">
                <td colspan="2"><input id="button2" type="SUBMIT" name="Word -> Song" value="Song to Word"></td>
            </tr>
            <tr align="center">
                <td colspan="2"><input id="button3" type="SUBMIT" name="Image -> Song" value="Song to Image"></td>
            </tr>
            </form>
            <tr align="center" valign="bottom" height="120px">
                <td colspan="2"><button id="buttonCancel" type="button" onclick="return cancel()" value="cancel">Theme selection</button></td>
            </tr>
        </table>
    </div>
    
    <script>
        function cancel(){
            document.location.href="http://localhost/accueil.php";
        }
    </script>
</body>
</html>
