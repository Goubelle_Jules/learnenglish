La première page à ouvrir est : accueil.php

Toutes les pages sont en drag and drop.
Le format est : /<type d'exercice>?id=<nom de la leçon>&theme=<nom du thème>
Exemple : emplacement/Word_Image.php?id=LeçonAnimals&theme=Animals

Le <nom de la leçon> correspond au nom du dossier utilisé comme conteneur pour les mots/sons/images.
Ceux-ci doivent être numérotés correctement : word<1-4>.png / song<1-4>.m4a / img<1-4>.jpg


Pour insérer un nouveau thème :
1. Créer un dossier avec 4 images, 4 sons, 4 mots qui respectent le format indiqué ci-dessus

2. Créer dans ce dossier un fichier txt intitulé 'theme.txt' qui suit le modèle ci-dessous :
id=<nom de la leçon>
theme=<nom du thème>
img src="<lien vers une image ou un gif qui illustre ce thème>"
alt="<description rapide de l'img src ci-dessus>"

exemple :
id=LeçonNoel
theme=Noel
img src="http://1.bp.blogspot.com/-bmRK1CdBLKQ/UNSBd1J0SLI/AAAAAAAAA4A/TusQ_LiTTMU/s1600/Globo-Natal-05.gif"
alt="boules de noel"

3. Compresser ce dossier un fichier .zip

4. Insérer ce dossier .zip en cliquant sur "Choisir un fichier" et sur "Envoyer"