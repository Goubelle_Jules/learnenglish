<!DOCTYPE html>
<!--Pour voir le détail des commentaires se baser sur le fichier Image_Word.php qui est commenté -->
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Learn English</title>

    <link rel="stylesheet" href="Style.css">

    <style>
        h2 {
            color: #EB5A5A;
            font-size: 4ch;
        }

        #main_table {
            margin-top: 2%;
        }
        
        button {
            font-family: "kid_font";
            padding: 16px 32px;
            font-size: 14px;
            margin: 4px 2px;
            cursor: pointer;
        }

        #jpg img {
            border: solid;
            color: #f44336;
            margin: 4px 2px;

            width: 300px;
            height: 180px;
        }

    </style>

    <?php
        $lecon = htmlspecialchars($_GET["id"]);
    ?>

</head>
<body>

    <table align="center" width="100%">
        <tr>
            <td align="left"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
            <td width="35%" align="right"><h1>Learn English</h1></td>
            <td width="25%" align="left"><img id="english_flag" src="https://acegif.com/wp-content/gifs/british-flag-31.gif" alt="english_flag"></td>
            <td align="right"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
        </tr>
    </table>

    <div id="main">
        <table id="main_table" align="center" width="100%" height="300px">
            <tr align="center">
                <td colspan="2"><h2>Drag the sounds to the correct picture</h2></td>
            </tr>
            <tr align="center">
                <td id="jpg" colspan="2">
                <?php 
                    $i=0;
                    $tabl = ['\img1.jpg','\img2.jpg','\img3.jpg','\img4.jpg'];
                    while($i!=4){
                        $ran=random_int(0,3);
                        if($tabl[$ran]!=null){
                        $i=$i+1;
                        echo "<img src='$lecon$tabl[$ran]' id='img$ran' ondrop='drop(event)' ondragover='allowDrop(event)'>";
                        $tabl[$ran]=null;
                    };
                    }
                ?>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2">
                <?php 
                    $i=0;
                    $tabl = ['\song1.m4a','\song2.m4a','\song3.m4a','\song4.m4a'];
                    while($i!=4){
                        $ran=random_int(0,3);
                        if($tabl[$ran]!=null){
                        $i=$i+1;
                        echo "<audio controls='controls' id='song$ran' src='$lecon$tabl[$ran]' draggable='true' ondragstart='drag(event)' width='140'
                        height='180'></audio>";
                        $tabl[$ran]=null;
                    };
                    }
                ?>
                </td>
            </tr>
            <tr align="center" valign="bottom" height="40px">
                <td width="50%" align="right"><button type="button" onclick="return validate()" value="submit">Submit</button></td>
                <td align="left"><button type="button" onclick="return cancel()" value="Cancel">Cancel</button></td>
            </tr>
        </table>
    </div>

    <script>
        var score = 0;
        function allowDrop(ev)
        {
        ev.preventDefault();
        }
        
        function drag(ev)
        {
        ev.dataTransfer.setData("Text",ev.target.id);
        }
        
        function drop(ev)
        {
        ev.preventDefault();
        
        var data=ev.dataTransfer.getData("Text");
        console.log(ev.target.id.charAt(ev.target.id.length-1));
        console.log(data.charAt(data.length-1));
        if(ev.target.id.charAt(ev.target.id.length-1)==data.charAt(data.length-1)){
            score=score+1;
        }
        console.log(score);
        ev.target.appendChild(document.getElementById(data));

        }

        function validate(){
            if(score == 4){
                document.location.href="http://localhost/Success.php";
            }
            else {
                document.location.href="http://localhost/error.php?id=<?php echo $lecon; ?>&theme=<?php echo htmlspecialchars($_GET["theme"]); ?>";
            }
        }

        function cancel(){
            document.location.href="http://localhost/choix_exos.php?id=<?php echo $lecon; ?>&theme=<?php echo htmlspecialchars($_GET["theme"]); ?>";
        }

    </script>
</body>
</html>