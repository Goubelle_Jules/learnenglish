<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Learn English</title>

    <link rel="stylesheet" href="Style.css">

    <style>
        h2 {
            color: rgb(69, 62, 194);
            font-size: 4ch;
        }

        #table {
            margin-top: 4%;
        }
        
    </style>
</head>
<body>
    <table align="center" width="100%">
        <tr>
            <td align="left"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
            <td width="35%" align="right"><h1>Learn English</h1></td>
            <td width="25%" align="left"><img id="english_flag" src="https://acegif.com/wp-content/gifs/british-flag-31.gif" alt="english_flag"></td>
            <td align="right"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
        </tr>
    </table>
    

    <div id="main">
                <?php

                $numFolders = count( glob("./*", GLOB_ONLYDIR) );
                $tableWidth = 30 + $numFolders * 10; //adapter largeur du tableau en fonction du nombre de dossiers (thèmes)

                echo '
                <table id="table" align="center" width="'.$tableWidth.'%" height="300px">
                    <tr align="center">
                        <td colspan="'.$numFolders.'"><h2>Choose a theme</h2></td>
                    </tr>
                    <tr align="center">';

                $scan = scandir('./');
                //création d'un thème en fonction de chaque dossier et du fichier theme.txt présent dans chacun de ces dossiers
                foreach($scan as $folder) {
                    if (is_dir("./$folder")) {
                        if ($folder != "." && $folder != ".."){
                            $lines_array = file("./$folder\\theme.txt");

                            foreach($lines_array as $line) {
                                if (strpos($line, "id=") !== false) {
                                    echo '<td><a href="choix_exos.php?'.$line;
                                }

                                if(strpos($line, "theme=") !== false) {
                                    echo '&'.$line.'" class="theme">';
                                }
                                
                                if(strpos($line, "theme=") !== false) {
                                    list(, $new_line) = explode("=", $line);
                                    echo $new_line;
                                }

                                if (strpos($line, "img src=") !== false) {
                                    echo '<br><'.$line.' ';
                                }
                                if (strpos($line, "alt=") !== false) {
                                    echo ' '.$line.'></a></td>';
                                }
                            }
                        }
                    }
                }
                 
                ?>
                
            </tr>
            <form action="ajoutLecon.php" method="post" enctype="multipart/form-data">
            <table id="table" align="center">
                <tr align="center">
                    <td align="right"><input type="file" name="import" accept=".zip"></td>
                    <td align="left"><input type="submit" name="submit" value="Envoyer"></td>
                </tr>
            </table>
            </form>
        </table>
    </div>

</body>
</html>