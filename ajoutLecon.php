<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Learn English</title>

    <link rel="stylesheet" href="Style.css">

    <style>
        h2 {
            color: rgb(191, 68, 68);
            font-size: 4ch;
        }
        h3 {
            color: rgb(80, 160, 40);
            font-size: 4ch;
        }

        #main_table {
            margin-top: 6%;
        }
        
        #buttonCancel {
            font-family: "kid_font";
            padding: 16px 32px;
            font-size: 20px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <table align="center" width="100%">
        <tr>
            <td align="left"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
            <td width="35%" align="right"><h1>Learn English</h1></td>
            <td width="25%" align="left"><img id="english_flag" src="https://acegif.com/wp-content/gifs/british-flag-31.gif" alt="english_flag"></td>
            <td align="right"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
        </tr>
    </table>
    

    <div id="main">
        <table id="main_table" align="center" width="50%" height="300px">
            <tr align="center">
                <td>
                    <?php
                    if($_FILES["import"]["error"] > 0){
                        echo "<h2>Erreur d'importation (erreur : " . $_FILES["import"]["error"] . ")<br></h2>";
                    } else{
                        $file = basename($_FILES["import"]["name"]);
                    
                    $zip = new ZipArchive;
                        if ($zip->open($_FILES["import"]["tmp_name"]) === TRUE) {
                        $zip->extractTo("./");
                        $zip->close();
                            echo "<h3>Le thème a bien été importé</h3>";
                        } else {
                            echo "<h2>Erreur d'ouverture du ZIP</h2>";
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr align="center" valign="bottom" height="120px">
                <td colspan="2"><button id="buttonCancel" type="button" onclick="return cancel()" value="cancel">Theme selection</button></td>
            </tr>
        </table>
    </div>

    <script>
        function cancel(){
            document.location.href="http://localhost/accueil.php";
        }
    </script>

</body>
</html>