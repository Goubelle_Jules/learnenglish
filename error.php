<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Learn English</title>

    <link rel="stylesheet" href="Style.css">

    <style>
        h2 {
            color: #EA4848;
            font-size: 4ch;
        }
        .fail_icon {
            width: 35%;
        }

        #main_table {
            margin-top: 2%;
        }

        button {
            font-family: "kid_font";
            padding: 16px 32px;
            font-size: 20px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <table align="center" width="100%">
        <tr>
            <td align="left"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
            <td width="35%" align="right"><h1>Learn English</h1></td>
            <td width="25%" align="left"><img id="english_flag" src="https://acegif.com/wp-content/gifs/british-flag-31.gif" alt="english_flag"></td>
            <td align="right"><img class="cloud1" src="https://media4.giphy.com/media/Qrdep630dyOucGsEsB/source.gif" alt="cloud1"></td>
        </tr>
    </table>
    

    <div id="main">
        <table id="main_table" align="center" width="50%" height="300px">
            <tr align="center">
                <td colspan="2"><img class="fail_icon" src="https://images.fineartamerica.com/images/artworkimages/medium/2/cross-in-circle-in-red-tom-hill-transparent.png" alt="fail_icon"></td>
            </tr>
            <tr align="center">
                <td colspan="2"><h2>It's not quite right, try again!</h2></td>
            </tr>
            <tr align="center" valign="bottom" height="40px">
                <td width="50%" align="right"><button type="button" onclick="return tryAgain()" value="submit">Try again</button></td>
                <td align="left"><button id="buttonCancel" type="button" onclick="return cancel()" value="cancel">Theme selection</button></td>
            </tr>
        </table>
    </div>

    <script>

        function cancel(){
            document.location.href="http://localhost/accueil.php";
        }

        function tryAgain(){
            document.location.href="http://localhost/choix_exos.php?id=<?php echo htmlspecialchars($_GET["id"]); ?>&theme=<?php echo htmlspecialchars($_GET["theme"]); ?>";
        }

    </script>

</body>
</html>